> - **Prix public : 49 €**
> - Les membres du [Forum Premium](http://forumnet.ircam.fr/fr/produit/offre-premium-individuel/) bénéficient de 25% de remise. [Demandez le coupon de réduction](http://forumnet.ircam.fr/shop/fr/contactez-nous)
> - [Site](https://www.jjburred.com/software/factorsynth/index.html)

Factorsynth est un plugin MaxForLive créé par J.J. Burred, chercheur et développeur indépendant. Il utilise une technique d’apprentissage automatique (factorisation matricielle) pour décomposer un son en un ensemble d’éléments temporels et spectraux. En réorganisant et en modifiant ces éléments, il est possible d’effectuer de puissantes transformations sur vos clips, telles que la suppression ou création de notes ou de motifs, la randomisation de mélodies ou de timbres, la modification de structures rythmiques, le remixage de boucles en temps réel, la création de textures sonores complexes…

![](https://forum.ircam.fr/media/uploads/factorsynth_logo_text_graybk1.png) ![](https://forum.ircam.fr/media/uploads/fs_v1.0_screen-788x500.png)

La factorisation prend généralement quelques secondes et peut être effectuée pendant la réproduction du projet Live. Une fois la factorisation prête, le son peut être modifié en temps réel en éditant et en recombinant les éléments extraits. Il est possible également de charger un deuxième son et effectuer une forme avancée de synthèse croisée entre des éléments du premier son (appelé “master sound”) et des éléments du second son (appelé “x-syn sound”). Les composants générés peuvent être individuellement exportés sous forme de fichiers wave pour un traitement ultérieur. Destiné aux compositeurs de musique électronique, aux sound designers et aux interprètes, Factorsynth ouvre une nouvelle gamme de possibilités pour le studio ou la scène.

## Configuration requise
* Mac ou Windows
* Ableton Live 9 ou 10
* MaxForLive 7

## Utilisation de Max
Il est possible d’utiliser Factorsynth à partir de Max sans besoin d’Ableton Live.

![](https://forum.ircam.fr/media/uploads/fs_max2.png)

## Le projet Factorsynth
J.J. Burred a lancé le projet Factorsynth en 2014, tout d’abord en effectuant des recherches sur les applications créatives d’une méthode d’analyse de données appelée factorisation matricielle (d’où son nom). Depuis lors, il a publié plusieurs versions prototypes de la ligne de commande et de Max. Ces anciennes versions n’étaient pas capables de fonctionner en temps réel, mais ont été utilisées par plusieurs compositeurs de musique électronique et électroacoustique pour le montage sonore détaillé et la spatialisation.

## Quelques travaux récents qui ont utilisé Factorsynth :
* [Artaud Overdrive](https://soundcloud.com/emanuele-palumbo/artaud-overdrive) d’[Emanuele Palumbo](https://www.ircam.fr/person/emanuele-palumbo). Première au festival Manifeste de l’Ircam, Centre Pompidou, Paris, juin 2016.
* [L’Aura della Distanza](https://soundcloud.com/emanuele-palumbo/laura-della-distanza) d’[Emanuele Palumbo](https://www.ircam.fr/person/emanuele-palumbo/). Première au CNSMDP, Paris, janvier 2017.
* [Khorwa, Myalwa](https://www.youtube.com/watch?v=Y2FsVPMagew&t=0s) de [Mikhail Malt](https://www.ircam.fr/person/mikhail-malt/). Enregistré en 2017.
* [Where the Here and Now of Nowhere](https://soundcloud.com/maurizio-azzan/where-the-here-and-now-of-nowhere-is) Is de [Maurizio Azzan](https://www.ircam.fr/person/maurizio-azzan/). Première au festival Manifeste de l’Ircam, Centre Pompidou, Paris, juin 2018.* [Where the Here and Now of Nowhere](https://soundcloud.com/maurizio-azzan/where-the-here-and-now-of-nowhere-is) Is de [Maurizio Azzan](http://brahms.ircam.fr/composers/composer/22134/). Première au festival Manifeste de l’Ircam, Centre Pompidou, Paris, juin 2018.

> - Contact et support technique : software@jjburred.com
> - [Documentation](https://www.jjburred.com/software/factorsynth/download/factorsynth_user_manual.pdf)
> - Vidéos : [Factorsynth - A Max for Live device](https://www.youtube.com/watch?v=Vjbe_vJth38) et [Preview](https://www.youtube.com/embed/3LodkAEPEY4)
